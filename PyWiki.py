# -*- coding: utf-8 -*-
# In the name of GOD

import wikipedia
import csv

page = wikipedia.page(title="Public_holidays_in_Spain").html()
obj = wikipedia.BeautifulSoup(page, "lxml")
table = obj.find(lambda tag: tag.name == 'table')
headers = [header for header in table.find_all('th')][2:]
headers = [header.text for header in headers[:3]] + [header.span.a['title'].encode("utf-8") for header in headers[3:]]
rows = []

for row in table.find_all('tr'):
    rows.append([val.text.encode('utf8') for val in row.find_all('td')])

with open('output_file.csv', 'wb') as f:
    writer = csv.writer(f)
    writer.writerow(headers)
    writer.writerows(row for row in rows if row)
