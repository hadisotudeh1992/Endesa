# -*- coding: utf-8 -*-
# In the name of GOD

import csv
from geopy.geocoders import Nominatim
import json
import datetime
import requests
import ssl
import time

from functools import wraps
def sslwrap(func):
    @wraps(func)
    def bar(*args, **kw):
        kw['ssl_version'] = ssl.PROTOCOL_TLSv1
        return func(*args, **kw)
    return bar

ssl.wrap_socket = sslwrap(ssl.wrap_socket)

geolocator = Nominatim()

test_list = ["Castilla y León","Andalucía","Castilla-La Mancha","Aragón","Extremadura","Catalunya","Galicia","Comunidad Valenciana","Región de Murcia","Principado de Asturias","Navarra","Comunidad de Madrid","Canarias","País Vasco","Cantabria","La Rioja","Islas Baleares","Ceuta","Melilla"]

dic = {}
states = {}
prov = {}
first = True
with open("DATATHON_secuencial.csv","r") as source:
  rdr= csv.reader( source )
  with open("db.csv","w") as result:
    wtr = csv.writer( result )
    for x in rdr:
      r = x[0].split("|")
      if first:
        wtr.writerow( ('date','holiday','day_desc','year','season','month','day','day_date',r[2], r[3], r[5],r[6],r[8],r[9],r[11],r[12],r[14],r[15],r[17],r[18],r[20],r[21],r[23],r[24],r[26],r[27],r[29],r[30],r[32],r[33],r[35],r[36],r[38],r[39],r[41],r[42],r[44],r[45],r[47],r[48],r[50],r[51],r[53],r[54],r[56],r[57],r[59],r[60],r[62],r[63],r[65],r[66],r[68],r[69],r[71],r[72],r[74],r[75],"administrative_area_level_1","administrative_area_level_2","city","city_long","city_lat",r[77],r[78],r[79],r[80],r[81],r[82]) )
        first = False
      else:
        try:
          r[76] = r[76].encode("utf-8")
        except:
          r[76] = r[76]
        city = r[76]
        tmp = dic.get(r[76])
        if tmp:
          lat = tmp[0]
          lng = tmp[1]
        else:
          if r[76] is None:
            lat = None
            lng = None
          else:
            if r[76] == "LAS PALMAS DE G.C.":
              location = geolocator.geocode("LAS PALMAS",timeout=60)
            elif r[76] == "CABEZAS DE SAN JUAN (LAS)":
              location = location = geolocator.geocode("Las Cabezas de San Juan",timeout=60)
            elif r[76] == "SAN ILDEFONSO O LA GRANJA":
              location = geolocator.geocode("La Granja Ildefonso",timeout=60)
            elif r[76] == "S/C DE LA PALMA":
              location = geolocator.geocode("S/C DE LA PALMA",timeout=60)
            elif r[76] == "SAN FERNANDO":
              location = geolocator.geocode("SAN FERNANDO cadiz",timeout=60)
            elif r[76] == "CASTELLON LA PLANA/CASTELLO PL":
              location = geolocator.geocode("CASTELLON LA PLANA",timeout=60)
            else:
              location = geolocator.geocode(r[76]+" spain",timeout=60)
            if location is None:
              lat = None
              lng = None
              print ("None location is : " + str(r[76]))
            else:
              lat = location.latitude
              lng = location.longitude
              dic[r[76]]=[lat,lng]
        if states.get(city):
          autonomous_community = states[city]
          if prov.get(city):
            province = prov[city]
        else:
          headers = {'User-Agent': 'Mozilla/5.0',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                    'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
                    'Accept-Encoding': 'none',
                    'Accept-Language': 'en-US,en;q=0.8',
                    'Connection': 'keep-alive'}
          url = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyAiq1tnDhMKQT1aftO-ypxX5L2ZM8XEiBQ&latlng="+str(lat)+","+str(lng)
          autonomous_community = "Nowhere"
          province = "Nowhere"
          try:
            obj = requests.get(url,headers=headers,timeout=60).json()
          except:
            obj = None
          if obj!=[] and obj is not None:
            obj = obj["results"]
            try:
              obj = obj[0]
            except:
              continue
            obj = obj.get("address_components")
          if obj!=[] and obj is not None:
            for item in obj:
              if "administrative_area_level_2" in item.get("types"):
                province = item["long_name"]
              elif "administrative_area_level_1" in item.get("types"):
                autonomous_community = item["long_name"]
          if autonomous_community is None:
            autonomous_community = "Nowhere"
          if province is None:
            province = "Nowhere"
          try:
            autonomous_community = autonomous_community.encode("utf-8")
          except:
            autonomous_community = autonomous_community
          try:
            province = province.encode("utf-8")
          except:
            province=province
          if autonomous_community == "Euskadi":
            autonomous_community = "País Vasco"
          if autonomous_community == "Illes Balears":
            autonomous_community = "Islas Baleares"
          if province == "Euskadi":
            province = "País Vasco"
          if province == "Illes Balears":
            province = "Islas Baleares"
          if autonomous_community == "Cataluña":
            autonomous_community = "Catalunya"
          if autonomous_community is not None:
            if autonomous_community not in test_list and autonomous_community!="Nowhere":
              print ("autonomous is : " + str(autonomous_community) + " province is : " + str(province) +" city is : " + str(city))
          states[city] = str(autonomous_community)
          prov[city] = str(province)
        date = r[77].split("Q")[-1]
        year = date[-4:]
        month = date[:-4]
        date_str = str(year)+str(month)
        if date_str == '':
          reg_date = ''
        else:
          reg_date = int(date_str)
        year = r[0][:4]
        month = r[0][4:6]
        day_date = r[0][6:]
        month = int(month)
        if month == 12 or month == 1 or month == 2:
          season = "winter"
        elif month == 3 or month == 4 or month == 5:
          season = "spring"
        elif month == 6 or month == 7 or month == 8:
          season = "summer"
        elif month == 9 or month == 10 or month == 11:
          season = "autumn"
        mydate = datetime.date(int(year),int(month),int(day_date))
        day = mydate.strftime("%A")
        holiday = False
        day_desc=None
        with open("calendar.csv","r") as cal:
          cl = csv.reader( cal )
          for j in cl:
            if r[0] == j[0]:
              if j[3]=="" and j[4]=="":
                if j[5]!="other":
                  holiday = True
                day_desc = str(j[2])
              elif autonomous_community is not None:
                if autonomous_community in j[3]:
                  if j[5]!="other":
                    holiday = True
                  day_desc = str(j[2])
              elif province is not None:
                if province in j[4]:
                  if j[5]!="other":
                    holiday = True
                  day_desc = str(j[2])
          if day == "Sunday" or day == "Saturday":
            holiday = True
            if day_desc == None:
              day_desc = "weekend"
          wtr.writerow( (int(r[0]),holiday,day_desc,int(year),str(season),int(month),str(day),int(day_date),r[2], r[3], r[5],r[6],r[8],r[9],r[11],r[12],r[14],r[15],r[17],r[18],r[20],r[21],r[23],r[24],r[26],r[27],r[29],r[30],r[32],r[33],r[35],r[36],r[38],r[39],r[41],r[42],r[44],r[45],r[47],r[48],r[50],r[51],r[53],r[54],r[56],r[57],r[59],r[60],r[62],r[63],r[65],r[66],r[68],r[69],r[71],r[72],r[74],r[75],str(autonomous_community),str(province),str(r[76]),lng,lat,reg_date,r[78],str(r[79]),r[80],r[81],r[82]) )

#tp = pd.read_csv("DATATHON_secuencial.csv", nrows=9999)
#tp.to_csv('db.csv')

#csvfile = open('db.csv', 'wb')
#try:
#    writer = csv.writer(csvfile)
#    writer.writerow( tp[0])
#    for i in range(10):
#        writer.writerow( tp[i+1] )
#finally:
#    csvfile.close()
