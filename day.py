import datetime
import csv
import json

import ssl
from functools import wraps
def sslwrap(func):
    @wraps(func)
    def bar(*args, **kw):
        kw['ssl_version'] = ssl.PROTOCOL_TLSv1
        return func(*args, **kw)
    return bar

ssl.wrap_socket = sslwrap(ssl.wrap_socket)

file = "Calendar_2015.csv"
year = 2015
first = True
with open(file,"r") as source:
  rdr= csv.reader( source )
  with open("new.csv","w") as result:
    wtr= csv.writer( result )
    for x in rdr:
      if first:
        wtr.writerow( ("date","day","title","location","type" ))
        first = False
        continue
      else:
        r = x
        date = r[0].split(" ")
        day = date[0]
        if int(day) < 10:
          day = str(0)+str(day)
        month = date[1]
        monthes = {"Jan":'01',"Feb":'02',"Mar":'03',"Apr":'04',"May":'05',"Jun":'06',"Jul":'07',"Aug":'08',"Sep":'09',"Oct":10,"Nov":11,"Dec":12}
        mydate = datetime.date(year,int(monthes[month]),int(day))
        d = mydate.strftime("%A")
        wtr.writerow((str(year)+str(monthes[month])+str(day),str(d),r[1],r[2],r[3]))
