# -*- coding: utf-8 -*-
# In the name of GOD

#min is : 20130912
#max is : 20151019
#number of cities : 808

import csv

import ssl
from functools import wraps
def sslwrap(func):
    @wraps(func)
    def bar(*args, **kw):
        kw['ssl_version'] = ssl.PROTOCOL_TLSv1
        return func(*args, **kw)
    return bar

ssl.wrap_socket = sslwrap(ssl.wrap_socket)

min = 20180000
max = 0
first = True
with open("DATATHON_secuencial.csv","r") as source:
  rdr= csv.reader( source )
  for x in rdr:
    if first:
      first = False
      continue
    r = x[0].split("|")
    for i in range(25):
      if int(r[(3*i)+1])!=i+1:
        print (str(i+1) + " is : " + str(r[(3*i)+1]) )

#tp = pd.read_csv("DATATHON_secuencial.csv", nrows=9999)
#tp.to_csv('db.csv')

#csvfile = open('db.csv', 'wb')
#try:
#    writer = csv.writer(csvfile)
#    writer.writerow( tp[0])
#    for i in range(10):
#        writer.writerow( tp[i+1] )
#finally:
#    csvfile.close()
